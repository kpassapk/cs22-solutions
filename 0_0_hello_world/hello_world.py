# python3

import sys

class Greeter:
    def __init__(self, name):
        self.name = name

    def greet(self):
        print(f"Hello, {self.name}!")

if __name__ == "__main__":
    name = sys.stdin.read()

    greeter = Greeter(name.rstrip())

    greeter.greet()
