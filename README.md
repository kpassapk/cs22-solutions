# CS 22 Solutions

## Instrucciones para Build (BORRAME)

1. Hacer un fork de este repo, y dar permiso de lectura a los instructores: `kpassapk` y `creyes52`.

2. Bajar SourceTree o algún otro cliente de Git, y descargar el fork a un directorio local. (Asumiremos `~/src/cs22-solutions`.)

3. Correr el ejemplo usando Docker. En la linea de comandos: (e.g. PowerShell en Windows, Terminal o ITerm2 en Mac).

```

   $ cd ~/src/cs22-solutions
   $ docker pull python:3

```

Esto descargará la imagen de Python 3. Tomará varios minutos, mostrando lo siguiente en pantalla:

```

   3: Pulling from library/python
   f49cf87b52c1: Pull complete
   7b491c575b06: Pull complete
   b313b08bab3b: Pull complete
   51d6678c3f0e: Downloading [==========================>        ]  70.45MB/135MB
   ... 
   
```

Una vez termine, verás un mensaje como este:

```

   Digest: sha256:98149ed5f37f48ea3fad26ae6c0042dd2b08228d58edc95ef0fce35f1b3d9e9f
   Status: Downloaded newer image for python:3

```

Esto quiere decir que ahora tienes una versión reciente de Python disponible. Verifica con:

```

    $ docker run -it --rm python:3 python --version
    Python 3.6.4

```

La primera parte dice que queremos correr un comando de manera interactiva (`-i`), quitando un contenedor intermedio (`--rm`) al terminar. Luego, tenemos el comando en si: `python --version`. Para correrlo más fácil, vamos a crear un alias:

```

    $ alias dpython3='docker run -it --rm -v $(pwd):/code -w /code python:3 python'
    
```

A ete alias, le agregamos dos switches adicionales. `-v $(pwd):/code` y `-w /code` especifican que queremos darle acceso a Python al directorio actual. Más info en la documentación de Docker.

Ahora lo puedes correr como `dpython3`:

```

    dpython3 --version
    Python 3.6.4

```

Para correr el ejemplo 0:

```

   $ dpython3 python 0_0_hello_world/hello_world.py

```

La terminal se va a quedar esperando un input. Escribir algo, luego enviar un EOF (*End Of File*) usando `Ctrl+D`.

```

    Quetzalcoatl
    Hello, Quetzalcoatl!

```

IMPORTANTE: Este ejemplo NO correrá con Python 2.7, solo con 3.6+.
Más información aquí: https://hub.docker.com/_/python/

4. Habilitar Bitbucket Pipelines. 

Más información aquí:

https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html

5. Ver el build fallar en Bitbucket Pipelines.


6. En el editor de texto (e.g. Visual Studio Code, Atom, Emacs, Vim) hacer cambios a el archivo `0_0_hello_world/hello_world.py` hasta que el output haga match con `0_0_hello_world/tests/01.a` cuando le doy el input especificado en `0_0_hello_world/tests/01`.

7. En SourceTree (o cualquier cliente de `git`) poner un *tag* para esta entrega. En este caso, puede ser `0_0_hello_world`. 

8. Hacer un "push" a el origen en Bitbucket, incluyendo los tags.

7. En Bitbucket Pipelines, verificar que este ejemplo pase el *build*. Esto quiere decir que obtiene el output correcto, sin tomar más tiempo que el máximo admisible, y sin que se acabe la memoria de la máquina.

8. Una vez pasa, hacer un submission: clonar el repo `cs22-submissions` y hacer un pull request con tu usuario y el nombre del tag:

```

    {usuario-en-bitbucket}
    └── {tag}
        └── bitbucket-pipelines.yml

```

por ejemplo, si yo soy `creyes52` y mi tag se llama '0_0_hello-world':

```

    cs22-submissions
    ├── creyes52
    │   └── 0_0_hello-world
    │       └── bitbucket-pipelines.yml
    ... 
    └── student1
        └── 0_0_hello-world
            └── bitbucket-pipelines.yml

```

Los nombres de los tags quedan a mi disposición. Si hago varios submissions para el mismo problema, terminaré con varios directorios:

```

    ...
    ├── student1
    │   ├── 0_0_hello-world
    │   │   └── bitbucket-pipelines.yml
    │   └── 0_0_hello-world-2
    │       └── bitbucket-pipelines.yml
    ...

```

`cs22-submissions` también tiene las entregas de los demás estudiantes. Tendré que hacer un "pull" (en SoureTree o tu cliente de `git`) antes de hacer mi propia entrega.

9. Borrar este mensaje, y ponerle algo más descriptivo de tu trabajo.

## FAQ

Me sale lo siguiente: 

```

    Warning: failed to get default registry endpoint from daemon (Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?). Using system default: https://index.docker.io/v1/
    Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?

```

Qué hago?

- Asegura que la última versión de Docker esté corriendo en tu máquina.

